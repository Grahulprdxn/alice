/* Author: 

*/
var named = document.querySelector(".name input");
var emailField = document.querySelector(".email input");
var textarea = document.querySelector("textarea");
var phone = document.querySelector(".phone input");

var letters = /^[a-zA-Z]+$/;
var reg = /^\w+@\w+\.\w{3}$/;
var tel = /\d{10}/;

document.querySelector("form button").addEventListener("click", function() {

	// FOR NAME FIELD STARTS HERE
	if(named.value == ""){
		var mistake = document.createElement("p");
		var mistext = document.createTextNode("Please fill the text field");
		mistake.appendChild(mistext);
		mistake.classList.add("wrong");
		var appending = document.getElementsByClassName("name")[0];
		appending.removeChild(appending.childNodes[2]);
		appending.appendChild(mistake);
		event.preventDefault();
	}
	else if(named.value.match(letters)) {
		var appending = document.getElementsByClassName("name")[0];
		appending.removeChild(appending.childNodes[2]);
	}
	else {
		var mistake = document.createElement("p");
		var mistext = document.createTextNode("Please enter aphabetical letters only");
		mistake.appendChild(mistext);
		mistake.classList.add("wrong");
		var appending = document.getElementsByClassName("name")[0];
		appending.removeChild(appending.childNodes[2]);
		appending.appendChild(mistake);
		event.preventDefault();
	}
	// FOR NAME FIELD ENDS HERE
  
  // FOR EMAIL FIELD STARTS HERE
  if(emailField.value == ""){
  	var emistake = document.createElement("p");
		var emistext = document.createTextNode("Please fill the email field");
		emistake.appendChild(emistext);
		emistake.classList.add("wrong");
		var appending2 = document.getElementsByClassName("email")[0];
		appending2.removeChild(appending2.childNodes[2]);
		appending2.appendChild(emistake);
		event.preventDefault();
  }
  else if (reg.test(emailField.value) == false) {
  	var emistake = document.createElement("p");
		var emistext = document.createTextNode("Invalid Email Address");
		emistake.appendChild(emistext);
		emistake.classList.add("wrong");
		var appending2 = document.getElementsByClassName("email")[0];
		appending2.removeChild(appending2.childNodes[2]);
		appending2.appendChild(emistake);
		event.preventDefault();
    return false;
  }
  else {  	
  	var appending2 = document.getElementsByClassName("email")[0];
		appending2.removeChild(appending2.childNodes[2]);
  	return true;
  }
  // FOR EMAIL FIELD STARTS HERE

 	// FOR TEXTAREA STARTS HERE
  if(textarea.value == ""){
  	var emistake = document.createElement("p");
		var emistext = document.createTextNode("Please fill the message field");
		emistake.appendChild(emistext);
		emistake.classList.add("wrong");
		var appending2 = document.getElementsByClassName("message")[0];
		appending2.removeChild(appending2.childNodes[2]);
		appending2.appendChild(emistake);
		event.preventDefault();
  }
  else {  	
  	var appending2 = document.getElementsByClassName("message")[0];
		appending2.removeChild(appending2.childNodes[2]);
  	return true;
  }
  // FOR TEXTAREA ENDS HERE

  // FOR PHONE FIELD STARTS HERE
  if(phone.value == ""){
		var mistake = document.createElement("p");
		var mistext = document.createTextNode("Please fill the phone number");
		mistake.appendChild(mistext);
		mistake.classList.add("wrong");
		var appending = document.getElementsByClassName("phone")[0];
		appending.removeChild(appending.childNodes[2]);
		appending.appendChild(mistake);
		event.preventDefault();
	}
	else if(phone.value.match(tel)) {
		var appending = document.getElementsByClassName("phone")[0];
		appending.removeChild(appending.childNodes[2]);
	}
	else {
		var mistake = document.createElement("p");
		var mistext = document.createTextNode("Please enter valid phone number only");
		mistake.appendChild(mistext);
		mistake.classList.add("wrong");
		var appending = document.getElementsByClassName("phone")[0];
		appending.removeChild(appending.childNodes[2]);
		appending.appendChild(mistake);
		event.preventDefault();
	}
	// FOR PHONE FIELD ENDS HERE

});

document.querySelector(".back-to-top button").addEventListener("click", function() {
  document.documentElement.scrollTop = 0;
});



















